
import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PokedexApi} from '../api/pokedex.api';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-pokedex-page',
  templateUrl: './pokedex.page.html',
  styleUrls: ['./pokedex.page.scss']
})
export class PokedexPage implements  OnInit {
  public pokemonList: Pokemon[];

  constructor(private readonly pokedexApi: PokedexApi) { }

  public ngOnInit() {
    this.pokedexApi.fetchPokemon$().subscribe((res: {results: Pokemon[]}) => {
      this.pokemonList = res.results;
    });
  }


}
