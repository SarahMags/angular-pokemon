
import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PokedexApi} from '../api/pokedex.api';
import {Pokemon} from '../../shared/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html'
})
export class PokemonListItemComponent implements  OnInit {
  @Input() name: string;
  @Input() id: string;

  public pokemonList: Pokemon[];

  constructor(private readonly pokedexApi: PokedexApi) { }

  public ngOnInit() {
  }


}
