import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {PokedexPage} from './pages/pokedex.page';
import {PokedexRoutingModule} from './pokedex-routing.module';
import {PokemonListItemComponent} from './components/pokemon-list-item.component';

@NgModule({
  declarations: [
    PokedexPage,
    PokemonListItemComponent
  ],
  imports: [
    SharedModule,
    PokedexRoutingModule
  ]
})
export class PokedexModule {
}
