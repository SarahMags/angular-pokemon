import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Pokemon} from '../../shared/models/pokemon.model';

const {pokedexApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokedexApi {
  constructor(private readonly http: HttpClient) {
  }

  public fetchPokemon$(): Observable<any> {
    return this.http.get(`${ pokedexApiBaseUrl }/pokemon?limit=21`);
  }

  /*public getImage$(id: string): Observable<any> {
    return this.http.get(`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`);
  }*/
}
