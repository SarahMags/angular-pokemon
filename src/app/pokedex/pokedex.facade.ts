import {Injectable} from '@angular/core';
import {PokedexApi} from './api/pokedex.api';

@Injectable({
  providedIn: 'root'
})
export class PokedexFacade {
  constructor(private readonly pokedexAPI: PokedexApi) {
  }
}
