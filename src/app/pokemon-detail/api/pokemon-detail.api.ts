import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const {pokedexApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailApi {
  constructor(private readonly http: HttpClient) {
  }
  public fetchPokemonDetail$(): Observable<any> {
    return this.http.get(`${ pokedexApiBaseUrl }/pokemon/1`);
  }

}
