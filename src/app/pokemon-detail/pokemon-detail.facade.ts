import {Injectable} from '@angular/core';
import {PokemonDetailApi} from './api/pokemon-detail.api';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailFacade {
  constructor(private readonly pokemonDetailApi: PokemonDetailApi) {
  }
}
