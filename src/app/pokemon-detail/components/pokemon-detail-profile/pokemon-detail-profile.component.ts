import {Component, Input, OnInit} from '@angular/core';
import {Pokemon, PokemonAbility, PokemonMoves, PokemonStat, PokemonType} from '../../../shared/models/pokemon.model';
import {PokemonDetailApi} from '../../api/pokemon-detail.api';

@Component({
  selector: 'app-pokemon-detail-profile',
  templateUrl: './pokemon-detail-profile.component.html'
})
export class PokemonDetailProfileComponent implements OnInit {
  @Input() pokemon: Pokemon;
  public pokemonDetails: Pokemon[];
  constructor(private readonly pokemonDetailApi: PokemonDetailApi) {
  }
  public ngOnInit() {
  }
}
