import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../../shared/models/pokemon.model';
import {PokemonDetailApi} from '../api/pokemon-detail.api';

@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-detail.page.html'
})
export class PokemonDetailPage implements OnInit {
  public pokemonDetailItem: Pokemon;
  constructor(private readonly pokemonDetailApi: PokemonDetailApi) {}
  public ngOnInit() {
    this.pokemonDetailApi.fetchPokemonDetail$().subscribe((res: {results: Pokemon}) => {
      this.pokemonDetailItem = res.results;
    });
  }

}
