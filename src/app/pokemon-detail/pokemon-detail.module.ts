import {NgModule} from '@angular/core';
import {PokemonDetailPage} from './pages/pokemon-detail.page';
import {PokemonDetailProfileComponent} from './components/pokemon-detail-profile/pokemon-detail-profile.component';
import {SharedModule} from '../shared/shared.module';
import {PokemonDetailRoutingModule} from './pokemon-detail-routing.module';

@NgModule({
  declarations: [
    PokemonDetailPage,
    PokemonDetailProfileComponent
  ],
  imports: [
    SharedModule,
    PokemonDetailRoutingModule
  ]
})
export class PokemonDetailModule {
}
