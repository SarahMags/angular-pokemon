import {RouterModule, Routes} from '@angular/router';
import {PokemonDetailPage} from './pages/pokemon-detail.page';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: PokemonDetailPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PokemonDetailRoutingModule {
}
