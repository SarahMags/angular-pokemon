import {Component} from '@angular/core';
import {User} from '../../../shared/models/user.model';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html'
})
export class LoginPage {

  constructor(private readonly router: Router) {
  }

  handleLoginSuccess(user: User): Promise<boolean> {
    // Store in localstorage
    return this.router.navigate([ AppRoutes.Pokedex ]);
  }

}
