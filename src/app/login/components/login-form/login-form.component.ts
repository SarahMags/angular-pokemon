import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {LoginFacade} from '../../login.facade';
import {User} from '../../../shared/models/user.model';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})

export class LoginFormComponent implements OnDestroy {

  @Output() successful: EventEmitter<User> = new EventEmitter<User>();

  public username: string = '';

  private user$: Subscription;

  constructor(private readonly loginFacade: LoginFacade) {
    this.user$ = this.loginFacade.user$().subscribe((user: User) => {
      if (user === null) {
        return;
      }
      this.successful.emit(user);
    });
  }
  get loading$(): Observable<boolean> {
    return this.loginFacade.loading$();
  }

  public onLoginClick(): void {
    this.loginFacade.login(this.username);
  }
  ngOnDestroy(): void {
    this.user$.unsubscribe();
  }
}
