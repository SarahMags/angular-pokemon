import {NgModule} from '@angular/core';
import {LoginPage} from './pages/login-page/login.page';
import {LoginRoutingModule} from './login-routing.module';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    LoginPage,
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    LoginRoutingModule
  ]
})

export class LoginModule{}
