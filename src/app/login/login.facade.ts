import {Injectable} from '@angular/core';
import {LoginApi} from './api/login.api';
import {HttpErrorResponse} from '@angular/common/http';
import {LoginState} from './state/login.state';
import {finalize} from 'rxjs/operators';
import {User} from '../shared/models/user.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginFacade {
  constructor(private readonly loginAPI: LoginApi, private readonly loginState: LoginState) {
  }

  public user$(): Observable<User> {
    return this.loginState.getUser$();
  }

  public loading$(): Observable<boolean> {
    return this.loginState.getLoading$();
  }


  public login(username: string): void {
    if (username !== ''){
      this.loginState.setUser({username, collectedPokemonIds: []});
      localStorage.setItem('username', username);
    }

/*    this.loginState.setLoading(true);

    this.loginAPI.login(username)
      .pipe(
        finalize(() => {
          this.loginState.setLoading(false);
      })
      )
      .subscribe( (user: User) => {
        this.loginState.setUser(user);
      }, (error: HttpErrorResponse) => {
        console.error(error);
      }); */
  }

}
