import {NgModule} from '@angular/core';
import {ContainerComponent} from './components/container/container.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    ContainerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule,
    ContainerComponent
  ]
})
export class SharedModule {
}
