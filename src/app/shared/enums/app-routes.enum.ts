export enum AppRoutes {
  Login= 'login',
  Pokedex = 'pokedex',
  PokemonDetail = 'pokemon-detail',
  Profile = 'profile'

}
