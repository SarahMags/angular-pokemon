export interface Pokemon {

  id?: string;
  name?: string;
  url?: string;
  height?: number;
  weight?: number;
  baseExperience?: number;
  abilities?: PokemonAbility[];
  image?: string;
  stats?: PokemonStat [];
  types?: PokemonType [];
  moves?: PokemonMoves [];
}

export interface PokemonSprite{
  id: number;
}
export interface PokemonAbility{
  id: number;
}
export interface PokemonStat{
  id: number;
}
export interface PokemonType{
  id: number;
}
export interface PokemonMoves{
  id: number;
}


// pokemon: Pokemon[] = response.results

// pokemon: Pokemon = response.results

