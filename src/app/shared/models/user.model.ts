export interface User {
  username: string;
  collectedPokemonIds?: string [];
}
