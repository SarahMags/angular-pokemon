import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {PokedexModule} from './pokedex/pokedex.module';
import {LoginModule} from './login/login.module';
import {PokemonDetailModule} from './pokemon-detail/pokemon-detail.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PokedexModule,
    LoginModule,
    PokemonDetailModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
