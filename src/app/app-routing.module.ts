import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppRoutes} from './shared/enums/app-routes.enum';
import {LoginPage} from './login/pages/login-page/login.page';
import {PokedexPage} from './pokedex/pages/pokedex.page';
import {PokemonDetailPage} from './pokemon-detail/pages/pokemon-detail.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`
  },
  {
    path: AppRoutes.Login,
    component: LoginPage
  },
  {
    path: AppRoutes.Pokedex,
    component: PokedexPage
  },
  {
    path: AppRoutes.PokemonDetail,
    component: PokemonDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
