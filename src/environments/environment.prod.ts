export const environment = {
  production: true,
  pokedexApiBaseUrl: 'https://pokeapi.co/api/v2'
};
